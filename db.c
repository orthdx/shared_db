#include "db.h"

#define SEM_FILE "/sem_file"
#define SEM_MUTEX "/sem_mutex"
#define SEM_WRITEBLOCK "/sem_writeblock"

#define SHM_COUNT "/shm_count"
#define SHM_BUFFER "/shm_buffer"

static Record local_db[MAX_SIZE];
int changes[MAX_SIZE];
int has_changed = 0;

int *shared_buffer;
int *shared_rcount;
volatile int keep_reading = 1;

int shm_count;
int shm_buffer;

sem_t *sem_file;
sem_t *mutex;
sem_t *write_block;

pthread_t reader_thread;

static int read_db_from_file(Record *out);
static int write_db_to_file(Record *in);
static void *synchronization_thread(void *arg);
static void write_changes();
static void update_local_db(void);
static void reader_lock(void);
static void reader_unlock(void);
static void clear_shared_memory(void);
/*
 * Constructor
 * Initializes semaphores, shared memory areas and starts thread
 * responsible for data synchronization.
 */
void __attribute__((constructor)) my_init() {

  sem_unlink(SEM_FILE);
  sem_unlink(SEM_MUTEX);
  sem_unlink(SEM_WRITEBLOCK);

  shm_count = shm_open(SHM_COUNT, O_CREAT | O_RDWR, S_IRWXU | S_IRWXG);
  ftruncate(shm_count, sizeof(int));

  shared_rcount = (int *)mmap(NULL, sizeof(int), PROT_READ | PROT_WRITE,
                              MAP_SHARED, shm_count, 0);

  shm_buffer = shm_open(SHM_BUFFER, O_CREAT | O_RDWR, S_IRWXU | S_IRWXG);
  ftruncate(shm_buffer, sizeof(int) * MAX_SIZE);

  shared_buffer =
      (int *)mmap(NULL, sizeof(int) * MAX_SIZE, PROT_READ | PROT_WRITE,
                  MAP_SHARED, shm_buffer, 0);

  sem_file = sem_open(SEM_FILE, O_CREAT, 00644, 1);
  mutex = sem_open(SEM_MUTEX, O_CREAT, 00644, 1);
  write_block = sem_open(SEM_WRITEBLOCK, O_CREAT, 0644, 1);

  pthread_create(&reader_thread, NULL, synchronization_thread, 0);

  load_database();
}

/*
 * Destructor
 * Joins thread and closes semaphores.
 */
void __attribute__((destructor)) my_fini() {
  keep_reading = 0;
  sem_post(mutex);

  pthread_join(reader_thread, NULL);

  sem_close(sem_file);
  sem_close(mutex);
  sem_close(write_block);
}

/*
 * Loads database from file to local_db variable
 */
int load_database() {
  sem_wait(write_block);
  int ret_val = 1;
  ret_val = read_db_from_file(local_db);
  sem_post(write_block);

  return ret_val;
}

/*
 * Reads database from file to given variable
 */
int read_db_from_file(Record *out) {
  int ret_val = 1;
  FILE *ptr;
  sem_wait(sem_file);
  ptr = fopen(FILE_PATH, "rb");

  if (!ptr) {
    fprintf(stderr, "Unable to open file!\n");
    ret_val = 1;
  } else {
    fseek(ptr, 0, SEEK_END);
    if (ftell(ptr) != 0) {
      fseek(ptr, 0, SEEK_SET);
      Record record;
      for (int i = 0; i < MAX_SIZE; i++) {
        if (fread(&record, sizeof(Record), 1, ptr) != 0) {
          memcpy(&out[i], &record, sizeof(Record));
        } else {
          break;
        }
      }
      ret_val = 0;
    }
  }

  fclose(ptr);
  sem_post(sem_file);

  return ret_val;
}

/*
 * Saves database to file and if someone has been changed
 * calls function that informs other processes about changes
 */
int save_database(void) {
  int ret_val = 1;
  ret_val = write_db_to_file(local_db);

  if (has_changed) {
    write_changes();
    has_changed = 0;
    for (int i = 0; i < MAX_SIZE; i++) {
      changes[i] = 0;
    }
  }
  return ret_val;
}

/*
 * Writes given variable to file.
 */
int write_db_to_file(Record *in) {
  sem_wait(sem_file);
  FILE *ptr;
  ptr = fopen(FILE_PATH, "wb");
  int ret_val = 1;

  if (!ptr) {
    fprintf(stderr, "Unable to open file!\n");
    ret_val = 1;
  } else {
    if (local_db != 0) {
      for (int i = 0; i < MAX_SIZE; i++)
        fwrite(&in[i], sizeof(Record), 1, ptr);
    }

    ret_val = 0;
  }
  fclose(ptr);
  sem_post(sem_file);
  return ret_val;
}

/*
 * Changes single record.
 */
int change_record(int key, Record record) {
  sem_wait(write_block);
  int ret_val = 1;
  if (key < MAX_SIZE && record.a != local_db[key].a) {
    memcpy(&local_db[key], &record, sizeof(Record));
    changes[key] = 1;
    has_changed = 1;
    ret_val = 0;
  }
  sem_post(write_block);
  return ret_val;
}

/*
 * Prints database.
 */
void print(void) {
  sem_wait(write_block);
  for (int i = 0; i < MAX_SIZE; i++) {
    if (i % 5 == 0) {
      printf("\n");
    }
    printf("%.5d: %.8s \t", i, local_db[i].a);
  }
  printf("\n");
  sem_post(write_block);
}

/*
 * Function responsible for data synchronization.
 */
void *synchronization_thread(void *arg) {
  while (keep_reading) {
    reader_lock();
    update_local_db();
    reader_unlock();
  }
  return arg;
}

/*
 * Function checks for differences between local_db and db stored in file.
 * shared_buffer is shared array, value 1 if update is necessary, 0 if not.
 */
void update_local_db(void) {
  Record temp[MAX_SIZE];
  read_db_from_file(temp);
  for (int i = 0; i < MAX_SIZE; i++) {
    if (shared_buffer[i] == 1) {
      memcpy(&local_db[i], &temp[i], sizeof(Record));
    }
  }
}

/*
 * Lock function for synchronization threads.
 * Waits for access to shared variable.
 * Increases shared readers counter.
 */
void reader_lock(void) {
  sem_wait(mutex);
  *shared_rcount = *shared_rcount + 1;
  if (*shared_rcount == 1)
    sem_wait(write_block);
  sem_post(mutex);
}

/*
 * Unlock for synchronization threads.
 * Waits for access to shared variable.
 * Decreases shared reader counter.
 * If it's last reader - clears shared memory.
 */
void reader_unlock(void) {
  sem_wait(mutex);
  *shared_rcount = *shared_rcount - 1;
  if (*shared_rcount == 0) {
    clear_shared_memory();
    sem_post(write_block);
  } else {
    sem_post(mutex);
  }
}

/*
 * Clears shared memory.
 */
void clear_shared_memory(void) {
  for (int i = 0; i < MAX_SIZE; i++) {
    shared_buffer[i] = 0;
  }
}

/*
 * Writes local changes array to shared memory.
 */
void write_changes() {
  sem_wait(write_block);
  sem_post(mutex);
  memcpy(shared_buffer, changes, sizeof(int) * MAX_SIZE);
  sem_post(write_block);
}
