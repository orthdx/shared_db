#ifndef db_h__
#define db_h__

/* Simple database
 * Race condition was solved using semaphore.
 * Consistency of database is assured by using semaphores
 * and shared memory.
 */

#include <fcntl.h>
#include <pthread.h>
#include <semaphore.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/un.h>
#include <unistd.h>

#define MAX_SIZE 1024
#define FILE_PATH "test.bin"

struct Record {
  char a[8];
};
typedef struct Record Record;

/*
 * Functions return 1 on error.
 */

extern int load_database();
extern int save_database();
extern int change_record(int key, Record record);

extern void print(void);
#endif